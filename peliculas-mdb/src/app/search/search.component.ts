import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FetchMoviesService } from '../home/fetch-movies.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  title: string;
  movies = {};
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private fetchMovieService: FetchMoviesService, private location: Location) {
    this.activatedRoute.params.subscribe(params => {
      this.title = params['title'];
      this.fetchMovieService.getMovieByTitle(this.title).subscribe(data => {
        this.movies = data;
        console.log(this.movies);
      });
    });
  }

  ngOnInit() {
  }

  back() {
    this.location.back();
  }

}
