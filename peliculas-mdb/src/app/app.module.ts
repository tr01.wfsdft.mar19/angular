import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FetchMoviesService } from './home/fetch-movies.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AboutComponent } from './about/about.component';
import { AddimagepathPipe } from './addimagepath.pipe';
import { PeliculaDetailComponent } from './pelicula-detail/pelicula-detail.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    AboutComponent,
    AddimagepathPipe,
    PeliculaDetailComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    FetchMoviesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
