import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  titulo = 'PeliculasIMDB';
  movies = {};
  constructor(private router: Router) { }

  ngOnInit() {
  }

  getMovieByTitle(form: any) {
    this.router.navigate(['/search', form.value.title]);
  }

  suggest(event) {
    if (event.length > 3) {
      this.router.navigate(['/search', event]);
    }
  }

}
