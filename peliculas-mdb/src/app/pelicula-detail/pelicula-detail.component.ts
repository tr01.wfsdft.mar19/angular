import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FetchMoviesService } from '../home/fetch-movies.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pelicula-detail',
  templateUrl: './pelicula-detail.component.html',
  styleUrls: ['./pelicula-detail.component.css']
})
export class PeliculaDetailComponent implements OnInit {

  slug: string;
  movie: {};
  constructor(private activatedRoute: ActivatedRoute, private fetchMovieService: FetchMoviesService, private router: Router, private location: Location) {
    this.activatedRoute.params.subscribe(params => {
      this.slug = params['slug'];
      this.fetchMovieService.getMovieById(this.slug).subscribe(data => {
        this.movie = data;
      });
    });
  }

  ngOnInit() {
  }

  back() {
    this.location.back();
  }

}
