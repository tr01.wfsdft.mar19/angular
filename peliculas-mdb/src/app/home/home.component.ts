import { Component, OnInit } from '@angular/core';
import { FetchMoviesService } from './fetch-movies.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title: string;
  movies: [];

  constructor(private fetchMoviesService: FetchMoviesService, private router: Router, private activatedRoute: ActivatedRoute, private location: Location) {
    this.activatedRoute.params.subscribe(params => {
      if (params.title) {
        this.title = params['title'];
        this.fetchMoviesService.getMovieByTitle(this.title).subscribe(data => {
          this.movies = data['results'];
          console.log(this.movies);
        });
      } else {
        this.getMovies();
      }
    });
  }

  ngOnInit() {

  }

  getMovies() {
    this.fetchMoviesService.getMovies().subscribe(data => {
      this.movies = data['results'];
      console.log(this.movies);
    });
  }

  getMovieById(slug: string) {
    this.router.navigate(['/detail', slug]);
  }

  back() {
    this.location.back();
  }
}
