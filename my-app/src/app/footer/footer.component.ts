import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  datosCurso: any = {
    header: 'Primer ejercicio de angular',
    footer: 'Footer'
  };
  constructor() { }

  ngOnInit() {
  }

}
