import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FetchVideosService {
  url = 'http://127.0.0.1:3000/videos';
  constructor(private http: HttpClient) { }

  getVideos() {
    // any[] devuelve un array de cualquier tipo
    // si quisieramos recibir sólo arrays de json pondríamos en lugar de any[] -> Object[]
    return this.http.get<any[]>(this.url);
  }
}
