import { Component, OnInit } from '@angular/core';

import { FetchVideosService } from './fetch-videos.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  videos = [];
  constructor(private fetchVideosService: FetchVideosService) {
  }

  ngOnInit() {
    this.getVideos();
  }

  getVideos() {
    this.fetchVideosService.getVideos().subscribe(data => {
      data.data.forEach(elem => {
        elem = String(elem);
        elem = elem.split(',');
        this.videos.push(elem);
        console.log(this.videos);
      });
    });
  }

}
